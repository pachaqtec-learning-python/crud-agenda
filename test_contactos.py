import unittest
import json

from main import app, db
#from database.db import db

BASE_URL = "http://localhost:8080/api/contact"

class SignupTest(unittest.TestCase):

    def setUp(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['DEBUG'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///unit_test.db'
        self.app = app.test_client()
        db.drop_all()
        db.create_all()
        #self.db = db.get_db()

    def test_getALL(self):
        response = self.app.get(BASE_URL)
        self.assertEqual(200, response.status_code)

    def create(self):
        payload = json.dumps({
            "name":"Alan",
            "lastname": "Ruiz Montoya",
            "movil": 1233,
            "email": "arsssa@asda.com"
        })
        response = self.app.post(BASE_URL, headers={"Content-Type": "application/json"}, data=payload)
        self.assertEqual(200, response.status_code)

    def tearDown(self):
        pass


if __name__ == "__main__":
    unittest.main()
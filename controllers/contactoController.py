from flask import request
from flask_jwt_extended import jwt_required

import logging

# Módulos propios
from models import *
from flask_restx import Resource

class ContactoController(Resource):
    @jwt_required
    def get(self, id):
        logging.info("Solicitando los datos del usuario - Método GET")
        contact = Contacts.query.get_or_404(id)
        dataJson = contactSchema.dump(contact)
        logging.info(dataJson)
        return dataJson

    @jwt_required
    def put(self, id):
        data = request.get_json()
        contact = Contacts.query.filter_by(id=id).first()
        if "name" in data: contact.name = data["name"]
        if "lastname" in data: contact.lastname = data["lastname"]
        db.session.commit()

        contact = Contacts.query.filter_by(id=id).first()
        return contactSchema.dump(contact)

    @jwt_required
    def delete(self, id):
        contact = Contacts.query.filter_by(id=id).first()
        db.session.delete(contact)
        db.session.commit()
        return contactSchema.dump(contact)

class ContactoPostController(Resource):
    @jwt_required
    def get(self):
        contacts = Contacts.query.all()
        return contactsSchema.dump(contacts)

    @jwt_required
    def post(self):
        data = request.get_json()

        new_contact = Contacts(
            name=data['name'],
            lastname=data['lastname'],
            movil=data['movil'],
            email=data['email']
        )

        db.session.add(new_contact)
        db.session.commit()
        return contactSchema.dump(new_contact)
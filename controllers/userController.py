from flask import request

from models import *
from flask_restx import Resource

class UserPostController(Resource):

     def post(self):
        data = request.get_json()
        new_user = Users(**data)
        new_user.hash_password()

        db.session.add(new_user)
        db.session.commit()
        return userSchema.dump(new_user)
import datetime
from flask import request
from flask_jwt_extended import create_access_token

from models import *
from flask_restx import Resource

class LoginController(Resource):

    def post(self):
        # Obtener data de BODY
        data = request.get_json()
        email = data.get("email")
        password = data.get("password")

        # Obtener el registro del Usuario en base al Email
        # user = Users.objects.get(email=email)
        # user = Users.query.filter_by(email=email)
        # authorized = Users.check_password(password)
        authorized = True if password == "peru2018" else False

        if not authorized:
            return {'error': 'Email or password invalid'}, 401

        # Generamos el Token
        expires = datetime.timedelta(days=7)
        access_token = create_access_token(identity=str(1), expires_delta=expires)
        return {'token': access_token}, 200

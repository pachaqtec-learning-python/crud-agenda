from . import db, ma
from flask_bcrypt import generate_password_hash, check_password_hash

class Users(db.Model):
    __bind_key__ = 'tablasPG'
    id          = db.Column(db.Integer, primary_key=True)
    email       = db.Column(db.String(190), unique=True, nullable=False)
    password    = db.Column(db.String(190), nullable=False)
    activo      = db.Column(db.Boolean, unique=False, nullable=False, default=1)

    def hash_password(self):
        self.password = generate_password_hash(self.password).decode('utf8')

    def check_password(self, password):
        return check_password_hash(self.password, password)


class UserSchema(ma.Schema):
    class Meta:
        fields = ("id","email", "estado")

userSchema = UserSchema()
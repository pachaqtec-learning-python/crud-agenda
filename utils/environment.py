import os
import os.path
import logging

from distutils.util import strtobool

if os.path.isfile('env.py'):
    print ("Como existe importar archivo de ambiente de desarrollo")
    import env


class Environment():

    def settingsGeneral(self):
        return{
            'PORT': int(os.environ["PORTAPI"]),
            'DEBUG': strtobool(os.environ['DEBUG'])
        }

    def settingsLogging(self):
        return{
            'FILE_NAME': os.environ["FILE_NAME"],
            'LEVEL': int(os.environ['LEVEL'])
        }

    def settingsSQLAlchemy(self):
        return{
            'TRACK_MODIFICATIONS': strtobool(os.environ["TRACK_MODIFICATIONS"]),
            'ECHO': strtobool(os.environ['ECHO'])
        }

    def settingBD(self, nameBD):
        if nameBD.upper() == "MYSQL": return self.__settingsMYSQL()
        if nameBD.upper() == "PG": return self.__settingsPG()
        if nameBD.upper() == "SQLLITE": return self.__settingsSQLLITE()

    def __settingsSQLLITE(self):
        return {
            'PATH': os.environ["SQLLITE_PATH"],
        }

    def __settingsMYSQL(self):
        settings =  {
            'HOST': os.environ["MYSQL_HOST"],
            'DATABASE': os.environ["MYSQL_DATABASE"],
            'PORT': int(os.environ["MYSQL_PORT"]),
            'USER': os.environ["MYSQL_USER"],
            'PWD': os.environ["MYSQL_PASSWORD"],
        }

        logging.info("Configuración MYSQL")
        logging.info(settings)

        return settings

    def __settingsPG(self):
        return{
            'HOST': os.environ["PG_HOST"],
            'DATABASE': os.environ["PG_DATABASE"],
            'PORT': int(os.environ["PG_PORT"]),
            'USER': os.environ["PG_USER"],
            'PWD': os.environ["PG_PASSWORD"],
        }



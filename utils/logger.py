import logging, colorama, copy
from utils.environment import Environment
settingsLogging = Environment().settingsLogging()

class ColorFormatter(logging.Formatter):
    def format(self, record, *args, **kwargs):
        # specify colors for different logging levels
        LOG_COLORS = {
            logging.DEBUG: colorama.Fore.GREEN,
            logging.INFO: colorama.Fore.BLUE,
            logging.WARNING: colorama.Fore.YELLOW,
            logging.ERROR: colorama.Fore.RED,
            logging.CRITICAL: colorama.Fore.RED
        }

        new_record = copy.copy(record)
        if new_record.levelno in LOG_COLORS:
            # we want levelname to be in different color, so let's modify it
            new_record.levelname = "{color_begin}{level}{color_end}".format(
                level=new_record.levelname,
                color_begin=LOG_COLORS[new_record.levelno],
                color_end=colorama.Style.RESET_ALL,
            )
        # now we can let standart formatting take care of the rest
        return super(ColorFormatter, self).format(new_record, *args, **kwargs)


# Archivos iniciales para la configuración de logging
FILE_NAME = settingsLogging["FILE_NAME"]
TYPE_LEVEL =  int(settingsLogging["LEVEL"]) #20, 30, 40, 50
FORMATTER_FILE = logging.Formatter('%(asctime)s [%(filename)s:%(funcName)s:%(lineno)d][%(levelname)s] %(message)s', '%m/%d/%Y %H:%M:%S')
FORMATTER_CONSOLE = ColorFormatter('%(asctime)s [%(filename)s:%(funcName)s:%(lineno)d][%(levelname)s] %(message)s', '%m/%d/%Y %H:%M:%S')

# FILE HANDLER
fileHandler = logging.FileHandler(FILE_NAME, encoding = "UTF-8")
fileHandler.setFormatter(FORMATTER_FILE)

# CONSOLE HANDLER
consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(FORMATTER_CONSOLE)

# ADD HANDLER
logger = logging.getLogger()
logger.setLevel(TYPE_LEVEL)
logger.addHandler(fileHandler)
logger.addHandler(consoleHandler)

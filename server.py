from flask import Flask
from flask_restx import Api
from flask_bcrypt import Bcrypt
from flask_jwt_extended import JWTManager

import logging
# Configuración Inicial Flask
logging.info("INICIALIZANDO SERVER")
app = Flask(__name__)

app.config['JWT_SECRET_KEY'] = "HolaMensajedePruebas"
app.config['JWT_ALGORITHM'] = "HS256"

bcrypt = Bcrypt(app)
jwt = JWTManager(app)

api = Api(
        app,
        title='Alan Cornejo',
        version='2.0',
        description='A description largaaaaaa',
    )


from utils.environment import Environment
config = Environment().settingsGeneral()


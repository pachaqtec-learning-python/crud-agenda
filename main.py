
# Módulos Propios
import utils.logger
from server import *
from models import *

from controllers.contactoController import ContactoController, ContactoPostController
from controllers.userController import UserPostController
from controllers.loginController import LoginController

contacto = api.namespace('api', description='Contacto API')
contacto.add_resource(ContactoPostController, '/contact')
contacto.add_resource(ContactoController, '/contact/<int:id>')

user = api.namespace('api', description='User API')
user.add_resource(UserPostController, '/user')

login = api.namespace('api', description='Login API')
login.add_resource(LoginController, '/login')

if __name__ == "__main__":
	app.run(port=config['PORT'], debug=config['DEBUG'])
